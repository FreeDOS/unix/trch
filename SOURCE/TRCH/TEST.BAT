@echo off

echo -- here is our test file, TEST1.TXT
type test1.txt

echo -- replace tabs with '*'
.\trch ^I * < test1.txt

echo -- replace 'e' with 'X'
.\trch e X < test1.txt

echo -- replace end of line ('\n') with '$'
.\trch \n $ < test1.txt

echo.
